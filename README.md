# NodeJS Express CRUD PostgreSQL

#### Hal-hal yang harus dilakukan untuk menjalankan project ini :

1. Clone repositori --> `git clone https://gitlab.com/hendisantika/nodejs-express-crud-postgres.git`
2. Masuk ke folder nya --> `cd nodejs-express-crud-postgres`
3. Install dependency --> `npm install`
4. Jalankan command berikut untuk migration table dan seeder nya --> `sequelize db:migrate` kemudian `sequelize db:seed:all`
5. 

#### Package Yang digunakan :

1. express
2. sequelize (untuk orm )
3. pg (untuk database postgre)
4. connect flash (untuk flash session)
5. ejs (view engine)
6. body parser (untuk parsing data yang di request)
7. faker js (untuk seeder, data faker nya)


#### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

List Suppliers

![List Suppliers](img/suppliers.png "List Suppliers")

Add New Supplier

![Add New Supplier](img/add_supplier.png "Add New Supplier")

List Items

![List Items](img/items.png "List Items")

Add New Item

![Add New Item](img/add_item.png "Add New Item")

![Demo Live di heroku.](https://nodejs-express-crud-postgres.herokuapp.com/)